/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_strlen
*/
int my_strlen(const char *str)
{
    int i = -1;

    while (str[++i]);
    return (i);    
}
