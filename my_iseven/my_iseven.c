/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_iseven
*/

void my_putchar(char c);

void my_iseven(int n)
{
    if (n % 2 == 0)
    {
        my_putchar('E');
    } else {
        my_putchar('O');
    }
}
