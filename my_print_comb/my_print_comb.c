/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      print_comb
*/

void my_putchar(char c);

void my_print_comb(void)
{
    int i;
    int j;
    int k;
 
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 10; k++)
            {
                if (i < j && j < k)
                {
                    my_putchar(i + '0');
                    my_putchar(j + '0');
                    my_putchar(k + '0');
                    if (i < 7)
                    {
                        my_putchar(',');
                        my_putchar(' ');
                    } else {
                        return;
                    }
                }
                
            }
            
        }
        
    }
    
}
