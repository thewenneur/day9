/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      putnbr
*/

void my_putchar(char c);

static void my_printstr(char *str)
{
    --str;
    while (*(++str))
    {
        my_putchar(*str);
    }  
}

static int my_intlen(int n)
{
    int i = 0;
    if (n < 0)
    {
        ++i;
        n *= -1;
    }  
    while (n > 0)
    {
        ++i;
        n /= 10;
    }
    return i;
}

void my_putnbr(int n)
{
    int i = my_intlen(n);
    char tmp[i + 1];
    int j = i;

    if (n == -2147483648)
    {
        my_printstr("-2147483648");
        return;
    }
    if (n == 0)
    {
        my_putchar('0');
        return;
    }
    if (n < 0)
    {
        tmp[0] = '-';
        n *= -1;
    }

    while (n > 0)
    {
        tmp[--j] = ((n % 10)  + '0');
        n /= 10;
    }
    tmp[i] = 0;
    my_printstr(tmp);
  
}
