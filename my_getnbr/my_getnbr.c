/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      getnbr
*/

int my_getnbr(const char *str)
{
    long nbr = 0;
    int neg = 1;

    while (*str && (*str == '+' || *str == '-'))
    {
        if (*str == '-')
        {
            neg *= -1;
        } else if (*str == '+' && *(str + 1) > '0' && *(str + 1) < '9')
        {
            neg = 1;
        }
        ++str;
    }
    while (*str && *str >= '0' && *str <= '9')
    {
        nbr *= 10;
        nbr += *str - '0';
        str++;
    }
    nbr *= neg;
    if (nbr > +2147483647 || nbr < -2147483648)
        return (0);
    return (nbr);
}
