/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_sort_intarr
*/

static void my_swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void my_sort_intarr(int *array, int size)
{
    int i;

    for (i = 0; i < size; i += 1)
    {
        int j = 0;
        for (j = 0; j < size - i - 1; j += 1)
        {
          if(array[j] > array[j + 1])
          {
              my_swap(array + j, array + j + 1);
          }
        } 
    }
}
