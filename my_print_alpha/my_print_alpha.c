/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      the my_print_alpha function
*/

void my_putchar(char c);

void my_print_alpha(void) {
    for (char i = 'a'; i <= 'z'; i += 1)
    {
        my_putchar(i);
    }
    my_putchar('\n');
}
