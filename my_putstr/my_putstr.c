/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_putstr
*/

void my_putchar(char c);

void my_putstr(const char *str)
{
    while (*str)
    {
        my_putchar(*str);
        str++;
    }
    
}
