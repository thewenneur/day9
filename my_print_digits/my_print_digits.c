/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_print_digits
*/

void my_putchar(char c);

void my_print_digits(void)
{
    for (char c = '0'; c <= '9'; c += 1)
    {
        my_putchar(c);
    }
    
}
