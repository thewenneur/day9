/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_revstr
*/

static int my_strlen(const char *str)
{
    int i = -1;

    while (str[++i]);
    return (i);    
}

char *my_revstr(char *str)
{
    int str_len = my_strlen(str);
    int len = str_len / 2;
    int i = -1;
    char c;
    while (++i < len)
    {
        c = str[i];
        str[i] = str[(str_len - 1) - i];
        str[(str_len - 1) - i] = c;
    }
    return str;
}
