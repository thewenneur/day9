/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      print_comb2
*/

void my_putchar(char c);

void my_print_comb2(void)
{
    int i;
    int j;

    for (i = 0; i < 99; i += 1)
    {
        for (j = 0; j < 100; j += 1)
        {
            if (i < j)
            {
                my_putchar((i / 10) + '0');
                my_putchar((i % 10) + '0');
                my_putchar(' ');
                my_putchar((j / 10) + '0');
                my_putchar((j % 10) + '0');
                
                if (!(i == 98 && j == 99))
                {
                    my_putchar(',');
                    my_putchar(' ');
                }
                
            }
            
        }
        
    }
    
}
