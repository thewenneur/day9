/*
** ETNA PROJECT, 24/10/2022 by brouil_t
** /home/parallels/Documents/etna/pool/day9
** File description:
**      my_isneg
*/

void my_putchar(char c);

void my_isneg(int n)
{
    if (n < 0)
    {
        my_putchar('N');
    } else {
        my_putchar('P');
    }
    
    
}
